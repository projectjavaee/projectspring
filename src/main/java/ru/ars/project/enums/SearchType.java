package ru.ars.project.enums;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 14:08 25.05.2017
 */
public enum SearchType {
    TITLE, TEAM, TRAINER
}
