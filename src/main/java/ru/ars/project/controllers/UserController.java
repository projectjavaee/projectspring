package ru.ars.project.controllers;

import org.springframework.stereotype.Component;
import ru.ars.project.entities.authorization.User;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 00:27 26.05.2017
 */
@Component
public class UserController {
    private User user;

    public User getUser() {
        if (user == null){
            user = new User();
        }
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
