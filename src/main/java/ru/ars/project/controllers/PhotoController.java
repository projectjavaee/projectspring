package ru.ars.project.controllers;

import org.omnifaces.cdi.GraphicImageBean;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.ApplicationScope;
import org.springframework.web.context.annotation.RequestScope;
import ru.ars.project.entities.PhotoPlayer;
import ru.ars.project.logics.interfaces.PhotoSearch;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 03:24 28.05.2017
 */
//@Component
@GraphicImageBean
public class PhotoController {

    @Autowired
    private PhotoSearch photoSearch;

    public byte[] get(Long id) {
        return photoSearch.getPhotoPlayer(id);

    }
}
