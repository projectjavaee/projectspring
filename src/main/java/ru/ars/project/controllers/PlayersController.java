package ru.ars.project.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.ars.project.entities.Player;
import ru.ars.project.entities.Team;
import ru.ars.project.entities.Trainer;
import ru.ars.project.logics.interfaces.PlayerSearch;
import ru.ars.project.logics.service.SearchService;

import javax.inject.Named;
import java.util.List;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 13:53 24.05.2017
 */
@Component
@Scope("singleton")
public class PlayersController {
    private final PlayerSearch PLAYERSEARCH;
    private final SearchService SEARCHSERVICE;
    private List<Player> players;
    private Player player;

    @Autowired
    public PlayersController(PlayerSearch PLAYERSEARCH, SearchService SEARCHSERVICE) {
        this.PLAYERSEARCH = PLAYERSEARCH;
        this.SEARCHSERVICE = SEARCHSERVICE;
    }


    public List<Player> getPlayers() {
        if (players == null){
            players = PLAYERSEARCH.getPlayer();
        }
        return players;
    }

    public Player getPlayer() {
        if (player == null){
            player = new Player();
        }
        return player;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    //===========================================================
    //======================== Search ===========================
    //===========================================================

    public void searchPlayerByPosition(){
        players = PLAYERSEARCH.getPlayer(SEARCHSERVICE.getPosition());
    }

    public void searchPlayerByTeam(){
        players = PLAYERSEARCH.getPlayer(SEARCHSERVICE.getTeam());
    }

    public void searchPlayerByTrainer(){
        players = PLAYERSEARCH.getPlayer(SEARCHSERVICE.getTrainer());
    }

    public void searchPlayerByText(){
        switch (SEARCHSERVICE.getSearchType()){
            case TITLE:
                players = PLAYERSEARCH.getPlayer(SEARCHSERVICE.getText());
                break;
//
//            case TRAINER:
//                players = PLAYERSEARCH.getPlayer(new Trainer(SEARCHSERVICE.getText(), SEARCHSERVICE.getText()));
//                break;
//
//            case TEAM:
//                players = PLAYERSEARCH.getPlayer(new Team(SEARCHSERVICE.getText()));
//                break;
//
        }
    }
}
