package ru.ars.project.controllers;

import org.springframework.stereotype.Component;

import javax.inject.Named;
import java.util.Date;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 21:38 25.05.2017
 */
@Component
public class CalendarController {
    private Date date;

    public Date getDate() {
        return date;
    }
}
