package ru.ars.project.servlets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ars.project.controllers.PlayersController;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

import static java.lang.String.format;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 04:20 26.05.2017
 */
@WebServlet(name = "PhotoServlet",
        urlPatterns = "/photo")
public class PhotoServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(PhotoServlet.class);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException  {
        response.setContentType("image/jpg");
        try (OutputStream outputStream = response.getOutputStream()){

            int index = Integer.valueOf(request.getParameter("index"));
            PlayersController playersController = (PlayersController) getServletContext().getAttribute("playersController");
            byte[] image = playersController.getPlayers().get(index).getPhotoPlayer().getPhoto();
            response.setContentLength(image.length);
            outputStream.write(image);


        }catch (Exception e){
            LOGGER.error(format("Exception in PhotoServlet.processRequest(): %s", e));
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Other methods. Click on the + sign on the left to edit the code.">
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
    // </editor-fold>
}
