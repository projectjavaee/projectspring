package ru.ars.project.logics.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.ars.project.enums.SearchType;

import javax.faces.context.FacesContext;
import java.util.HashMap;
import java.util.Map;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 14:59 25.05.2017
 */
@Component
@Scope("singleton")
public class SearchCriteria {
    private Map<String, SearchType> searchTypeMap = new HashMap<>();
    private String search;

    private final MessageSource messageSource;

    @Autowired
    public SearchCriteria(MessageSource messageSource) {
        this.messageSource = messageSource;
    }


    public Map<String, SearchType> getSearchTypeMap() {
        searchTypeMap.clear();
        searchTypeMap.put(messageSource.getMessage("player_name", null, FacesContext.getCurrentInstance().getViewRoot().getLocale()), SearchType.TITLE);
        searchTypeMap.put(messageSource.getMessage("trainer_name", null, FacesContext.getCurrentInstance().getViewRoot().getLocale()), SearchType.TRAINER);
        searchTypeMap.put(messageSource.getMessage("team_name", null, FacesContext.getCurrentInstance().getViewRoot().getLocale()), SearchType.TEAM);
        return searchTypeMap;
    }

    public void setSearchTypeMap(Map<String, SearchType> searchTypeMap) {
        this.searchTypeMap = searchTypeMap;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

}
