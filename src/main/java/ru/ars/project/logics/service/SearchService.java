package ru.ars.project.logics.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.ars.project.entities.Position;
import ru.ars.project.entities.Team;
import ru.ars.project.entities.Trainer;
import ru.ars.project.enums.SearchType;

import javax.inject.Named;
import java.io.Serializable;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 14:06 25.05.2017
 */
@Component
@Scope("singleton")
public class SearchService implements Serializable{
    private String text;
    private SearchType searchType = SearchType.TITLE;
    private Position position;
    private Trainer trainer;
    private Team team;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public SearchType getSearchType() {
        return searchType;
    }

    public void setSearchType(SearchType searchType) {
        this.searchType = searchType;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }
}
