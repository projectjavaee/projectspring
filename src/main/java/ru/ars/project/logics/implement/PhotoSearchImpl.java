package ru.ars.project.logics.implement;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ars.project.entities.PhotoPlayer;
import ru.ars.project.entities.Player;
import ru.ars.project.logics.interfaces.PhotoSearch;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.List;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 03:11 28.05.2017
 */
@Repository
public class PhotoSearchImpl implements PhotoSearch, Serializable{

    @Autowired
    private SessionFactory sessionFactory;
    private List<PhotoPlayer> photoPlayers;

    @Override
    @Transactional
    public List<PhotoPlayer> getPhotoList() {

        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<PhotoPlayer> criteria = builder.createQuery(PhotoPlayer.class);
        Root<PhotoPlayer> p = criteria.from(PhotoPlayer.class);
        criteria.select(p);
        return session.createQuery(criteria).getResultList();
    }


    @Override
    @Transactional
    public byte[] getPhotoPlayer(Long id) {

        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<PhotoPlayer> criteria = builder.createQuery(PhotoPlayer.class);
        Root<PhotoPlayer> p = criteria.from(PhotoPlayer.class);
        criteria.select(p).where(builder.equal(p.get("id"), id));
        photoPlayers = session.createQuery(criteria).getResultList();
        if (photoPlayers.isEmpty()){
            return null;
        }
        return photoPlayers.get(0).getPhoto();

//        or

//        try {
//            return em.createQuery(criteria).getSingleResult().getPhoto();
//        }catch (NoResultException e){
//            return null;
//        }
    }
}
