package ru.ars.project.logics.implement;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.*;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.ars.project.logics.interfaces.PlayerSearch;
import ru.ars.project.entities.Player;
import ru.ars.project.entities.Position;
import ru.ars.project.entities.Team;
import ru.ars.project.entities.Trainer;

import java.util.List;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 19:24 21.05.2017
 */
@Repository
public class PlayerSearchImpl implements PlayerSearch {

    @Autowired
    private SessionFactory sessionFactory;
    private List<Player> players;
    private ProjectionList playerProjection;

    public PlayerSearchImpl() {
        playerProjection = Projections.projectionList();
        playerProjection.add(Projections.property("id"), "id");
        playerProjection.add(Projections.property("name"), "name");
        playerProjection.add(Projections.property("surname"), "surname");
        playerProjection.add(Projections.property("city"), "city");
        playerProjection.add(Projections.property("biography"), "biography");
        playerProjection.add(Projections.property("birthday"), "birthday");
        playerProjection.add(Projections.property("height"), "height");
        playerProjection.add(Projections.property("weight"), "weight");
        playerProjection.add(Projections.property("number"), "number");
        playerProjection.add(Projections.property("position"), "position");
        playerProjection.add(Projections.property("team"), "team");
        playerProjection.add(Projections.property("trainer"), "trainer");
        playerProjection.add(Projections.property("photoPlayer"), "photoPlayer");
    }

    /**
     * Метод возвращает всех players
     * @return упорядоченый по position список players
     */
    @Transactional
    @Override
    public List<Player> getPlayer() {
        // <editor-fold defaultstate="collapsed" desc="Analogue method. Click on the + sign on the left to edit the code.">
//        Session session = sessionFactory.getCurrentSession();
//        CriteriaBuilder builder = session.getCriteriaBuilder();
//        CriteriaQuery<Player> criteria = builder.createQuery(Player.class);
//        Root<Player> playerRoot = criteria.from(Player.class);
//        criteria.select(playerRoot);
//        players = session.createQuery(criteria).getResultList();
//        return players;// </editor-fold>
        players = createPlayerList(createPlayerCriteria());
        return players;

    }

    /**
     * Метод возвращает список players по заданному имени
     * @param playerName имя player
     * @return упорядоченый по position список players
     */
    @Transactional
    @Override
    public List<Player> getPlayer(String playerName) {
        // <editor-fold defaultstate="collapsed" desc="Analogue method. Click on the + sign on the left to edit the code.">
//        Session session = sessionFactory.getCurrentSession();
//        CriteriaBuilder builder = session.getCriteriaBuilder();
//        CriteriaQuery<Player> criteria = builder.createQuery(Player.class);
//        Root<Player> playerRoot = criteria.from(Player.class);
//        criteria.select(playerRoot).where(builder.equal(playerRoot.get("name"), playerName));
//        players = session.createQuery(criteria).getResultList();
//        return players;// </editor-fold>

        players = createPlayerList(createPlayerCriteria().add(Restrictions.ilike("p.name", playerName, MatchMode.ANYWHERE)));
        return players;
    }

    /**
     * Метод возвращает список players по заданной позиции
     * @param position позиция
     * @return упорядоченый по position список players
     */
    @Transactional
    @Override
    public List<Player> getPlayer(Position position) {
        // <editor-fold defaultstate="collapsed" desc="Analogue method. Click on the + sign on the left to edit the code.">
//        Session session = sessionFactory.getCurrentSession();
//        CriteriaBuilder builder = session.getCriteriaBuilder();
//        CriteriaQuery<Player> criteria = builder.createQuery(Player.class);
//        Root<Player> playerRoot = criteria.from(Player.class);
//        criteria.select(playerRoot).where(builder.equal(playerRoot.get("id_position"), position));
//        players = session.createQuery(criteria).getResultList();
//        return players;// </editor-fold>
        return createPlayerList(createPlayerCriteria().add(Restrictions.ilike("position.title", position.getTitle(), MatchMode.ANYWHERE)));
    }

    /**
     * Метод возвращает список players по заданному тренеру
     * @param trainer тренер
     * @return упорядоченый по position список players
     */
    @Transactional
    @Override
    public List<Player> getPlayer(Trainer trainer) {
        // <editor-fold defaultstate="collapsed" desc="Analogue method. Click on the + sign on the left to edit the code.">
//        Session session = sessionFactory.getCurrentSession();
//        CriteriaBuilder builder = session.getCriteriaBuilder();
//        CriteriaQuery<Player> criteria = builder.createQuery(Player.class);
//        Root<Player> playerRoot = criteria.from(Player.class);
//        criteria.select(playerRoot).where(builder.equal(playerRoot.get("id_trainer"), trainer));
//        players = session.createQuery(criteria).getResultList();
//        return players;// </editor-fold>
        return createPlayerList(createPlayerCriteria().add(Restrictions.ilike("trainer.name", trainer.getName(), MatchMode.ANYWHERE)));
    }

    /**
     * Метод возвращает список players по заданной команде
     * @param team команда
     * @return упорядоченый по position список players
     */
    @Transactional
    @Override
    public List<Player> getPlayer(Team team) {
        // <editor-fold defaultstate="collapsed" desc="Analogue method. Click on the + sign on the left to edit the code.">
//        Session session = sessionFactory.getCurrentSession();
//        CriteriaBuilder builder = session.getCriteriaBuilder();
//        CriteriaQuery<Player> criteria = builder.createQuery(Player.class);
//        Root<Player> playerRoot = criteria.from(Player.class);
//        criteria.select(playerRoot).where(builder.equal(playerRoot.get("id_team"), team));
//        players = session.createQuery(criteria).getResultList();
//        return players;// </editor-fold>
        return createPlayerList(createPlayerCriteria().add(Restrictions.ilike("team.name_team", team.getName(), MatchMode.ANYWHERE)));
    }

    /**
     *
     * @return DetachedCriteria
     */
    private DetachedCriteria createPlayerCriteria(){
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Player.class, "p");
        createAliases(detachedCriteria);
        return detachedCriteria;
    }


    /**
     * принимаемые параметры в методе - в таблице не должны равняться null,
     * иначе вся строка таблицы не попадет в выгружаемый список
     * @param criteria DetachedCriteria
     */
    private void createAliases(DetachedCriteria criteria){
        criteria.createAlias("p.position", "position");
        criteria.createAlias("p.team", "team");
        criteria.createAlias("p.trainer", "trainer");
    }


    /**
     * Метод создает из DetachedCriteria - List<Player> и сортирует его по параметру position
     * @param detachedCriteria DetachedCriteria
     * @return упорядоченый по position список players
     */
    private List<Player> createPlayerList(DetachedCriteria detachedCriteria){
        Criteria criteria = detachedCriteria.getExecutableCriteria(sessionFactory.getCurrentSession());
        criteria.addOrder(Order.asc("p.position")).setProjection(playerProjection).setResultTransformer(Transformers.aliasToBean(Player.class));
        return criteria.list();
    }
}
