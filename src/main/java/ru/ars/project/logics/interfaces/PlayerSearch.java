package ru.ars.project.logics.interfaces;

import ru.ars.project.entities.Player;
import ru.ars.project.entities.Trainer;
import ru.ars.project.entities.Position;
import ru.ars.project.entities.Team;

import java.util.List;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 04:33 20.05.2017
 */
public interface PlayerSearch {

    List<Player> getPlayer();
    List<Player> getPlayer(String playerName);
    List<Player> getPlayer(Position position);
    List<Player> getPlayer(Trainer trainer);
    List<Player> getPlayer(Team team);


}
