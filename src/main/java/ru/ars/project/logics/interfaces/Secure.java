package ru.ars.project.logics.interfaces;

import ru.ars.project.entities.authorization.User;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 04:37 20.05.2017
 */
public interface Secure {

    boolean login(User user);
    void logout(User user);
}
