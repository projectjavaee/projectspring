package ru.ars.project.logics.interfaces;

import ru.ars.project.entities.PhotoPlayer;

import java.util.List;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 02:55 28.05.2017
 */
public interface PhotoSearch {

    List<PhotoPlayer> getPhotoList();
    byte[] getPhotoPlayer(Long id);

}
