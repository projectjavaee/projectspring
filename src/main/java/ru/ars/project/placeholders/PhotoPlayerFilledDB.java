package ru.ars.project.placeholders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ars.project.entities.PhotoPlayer;
import ru.ars.project.logics.interfaces.PhotoSearch;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import java.util.List;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 00:39 03.06.2017
 */
@Component
@RequestScoped
public class PhotoPlayerFilledDB {

    private List<PhotoPlayer> photo;

    @Autowired
    private PhotoSearch photoSearch;

//    @PostConstruct
    public void init(){
        photo = photoSearch.getPhotoList();
    }

    public List<PhotoPlayer> getPhoto() {
        return photo;
    }
}