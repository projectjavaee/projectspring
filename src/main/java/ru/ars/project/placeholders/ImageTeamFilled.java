package ru.ars.project.placeholders;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 01:51 25.05.2017
 */
@Component
public class ImageTeamFilled {
    private List<String> images;

    @PostConstruct
    public void init(){
        images = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            images.add("team" + i + ".jpg");
        }
    }

    public List<String> getImages() {
        return images;
    }
}
